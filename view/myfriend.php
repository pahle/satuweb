<?php 
require '../assets/php/config.php';

function getQuery($params) {
    global $conn;
    $query=mysqli_query($conn, "$params");
   
    // exception handling
    if (!$query) {
        return mysqli_error($conn);
        exit;
    }

    // return
    $table = []; // membuat kotak array 
    while ($row = mysqli_fetch_assoc($query)) {
        $table[] = $row; // menambahkan tiap tiap baris
    }
    return $table; // mengembalikan array multi dimensi
}

function createQuery($params) {
    global $conn;

    $name = $params['name'];
    $phone = $params['phone'];
    $address = $params['address'];

    $create = "INSERT INTO friends VALUES (
        '',
        '$name',
        '$phone',
        '$address'

    )";

    mysqli_query($conn, $create);

    return mysqli_affected_rows($conn);
}

if (isset($_GET['id_t'])) {
	$id_target = $_GET['id_t'];
}
$target; $target_name; $target_phone; $target_address; 
if(isset($id_target)) {
    $target = getQuery("SELECT * FROM friends WHERE id=$id_target")[0];
    $target_name = $target['name'];
    $target_phone = $target['phone'];
    $target_address = $target['address']; 
}
function editData($params) {
	global $conn, $id_target;

    $name = $params['name'];
    $phone = $params['phone'];
    $address = $params['address'];

    $create = "UPDATE friends set
        name = '$name',
        phone = '$phone',
        address = '$address'
		WHERE id = $id_target
	";

    mysqli_query($conn, $create);

    return mysqli_affected_rows($conn);
}

$friends = getQuery("SELECT * FROM friends");

if(isset($_POST['submit'])) {

	if (isset($target)) {
		if (editData($_POST) > 0) {
			echo '
				<script>alert("data berhasil diubah!")</script>
			';
			header("location: myfriend.php");
		} else {
			echo '
				<script>alert("data gagal diubah!")</script>
			';
			header("location: myfriend.php");
		}
	} else {
		if (createQuery($_POST) > 0) {
			echo '
				<script>alert("data berhasil ditambahkan!")</script>
			';
			header("location: myfriend.php");
		} else {
			echo '
				<script>alert("data gagal ditambahkan!")</script>
			';
			header("location: myfriend.php");
		}
	}
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>MyFriends</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/c101a12428.js" crossorigin="anonymous"></script>
</head>

<body>
    <section class="main-content">
        <div class="container">
            <h1 class="text-center my-4">Users Table UI</h1>
            <br>

            <table class="table">
                <thead>
                    <tr>
                        <th class="ps-5">User</th>
                        <th>Location</th>
                        <th>Phone</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($friends as $friend) : ?>
                    <tr>
                        <td class="ps-5">
                            <div class="user-info">
                                <div class="user-info__basic">
                                    <h5 class="mb-0"><?= $friend['name'] ?></h5>
                                    <p class="text-muted mb-0">@<?= $friend['name'] ?></p>
                                </div>
                            </div>
                        </td>
                        <td>
                            <?= $friend['address'] ?>
                        </td>
                        <td><?= $friend['phone'] ?></td>
                        <td class="">
                            <div class="dropdown open">
                                <a href="#!" class="px-2" id="triggerId1" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                    <i class="fa fa-ellipsis-v"></i>
                                </a>
                                <div class="dropdown-menu" aria-labelledby="triggerId1">
                                    <a class="dropdown-item" href="myfriend.php?id_t=<?= $friend['id'] ?>"><i
                                            class="fa fa-pencil mr-1"></i> Edit</a>
                                    <a class="dropdown-item text-danger"
                                        href="../assets/php/delete.php?i=<?= $friend['id'] ?>"><i
                                            class="fa fa-trash mr-1"></i>
                                        Delete</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </section>

    <div class="w-50 m-auto border border-gray p-4 px-5">
        <h2 class="text-center mb-3">myFriends Input Form</h2>
        <form action="" method="post">
            <div class="mb-3">
                <label for="exampleInputname1" class="form-label">Name</label>
                <input type="text" name="name" class="form-control" id="exampleInputname1" aria-describedby="nameHelp"
                    autocomplete="off" required value="<?php if (!empty($target)) {
						echo $target_name;
					} ?>">
                <div id="nameHelp" class="form-text">Input your name</div>
            </div>
            <div class="mb-3">
                <label for="exampleInputphone1" class="form-label">Phone</label>
                <input type="text" name="phone" class="form-control" id="exampleInputphone1"
                    aria-describedby="phoneHelp" autocomplete="off" required value="<?php if (!empty($target)) {
						echo $target_phone;
					} ?>">
                <div id="phoneHelp" class="form-text"></div>
            </div>
            <div class="mb-4">
                <label for="exampleFormControlTextarea1" class="form-label">Address</label>
                <textarea class="form-control" name="address" id="exampleFormControlTextarea1" rows="3"><?php if (!empty($target)) {
						echo $target_address;
					} ?></textarea>
            </div>
            <button type="submit" name="submit" class="btn btn-primary px-4">Submit</button>
        </form>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>

</html>